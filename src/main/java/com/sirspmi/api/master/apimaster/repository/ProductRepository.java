package com.sirspmi.api.master.apimaster.repository;

import com.sirspmi.api.master.apimaster.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductRepository extends CrudRepository<Product, Long> {
}