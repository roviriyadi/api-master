package com.sirspmi.api.master.apimaster.controller;

import java.util.List;
import java.util.Optional;

import com.sirspmi.api.master.apimaster.entity.Product;
import com.sirspmi.api.master.apimaster.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductRepository repository;

    @GetMapping("/product")
    Iterable<Product> findAll() {
        
        Iterable<Product> products = repository.findAll();

        return products;

    }

    @GetMapping("/product/{id}")
    Optional<Product> findById(@PathVariable Long id) {
        
        Optional<Product> product = repository.findById(id);

        return product;

    }

}