package com.sirspmi.api.master.apimaster.repository;

import com.sirspmi.api.master.apimaster.entity.Product;
import org.springframework.stereotype.Component;

@Component
public interface ProductJpaRepository extends JpaRepository<Product, Long> {
}