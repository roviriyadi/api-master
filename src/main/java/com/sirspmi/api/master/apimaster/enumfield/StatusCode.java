package com.sirspmi.api.master.apimaster.enumfield;

public enum StatusCode {
    NEW, SECOND, BROKEN, OTHER;
}