package com.sirspmi.api.master.apimaster.repository;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Sort;

public interface JpaRepository<T, ID extends Serializable> extends PagingAndSortingRepository<T, ID> {

    List<T> findAll();

    List<T> findAll(Sort sort);

    List<T> save(Iterable<? extends T> entities);

    void flush();

    T saveAndFlush(T entity);

    void deleteInBatch(Iterable<T> entities);

}